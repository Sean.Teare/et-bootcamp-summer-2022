import React, {useState} from 'react'
import { Input, Button } from '@mui/material'

export const Form = () => {
  const [firstName, setFirstName] = useState('')
  const handleNameChange = ({ target }) => {
    console.log("name change", target)
    setFirstName(target.value)
  }

  const submitForm = () => {
    console.log('My Data', 'firstName', firstName)
  }
  return (
    <div>
      <h1>Hello There</h1>
      <Input type="text" onChange={handleNameChange} value={firstName} name="firstName"/>
      <Button onClick={submitForm} variant="contained">Click Me</Button>
    </div>
  )
}