import React, { useEffect, useState } from 'react'
import { getProducts } from '../services/content'
/**
 * Requirements
 * 
 * Cart that allows a user to select from a list of products (ally products) and add them to a cart
 * Display the cart contents beside the form
 * 
 * CartContents - List of products that have already been added to the cart.  items={items}
 *   list of Item
 *   Summary of cart contents
 * ProductForm - Form for adding things to the list  - products={products} 
 *   
 *     QuantityBox -> input type number ()
 *     Select (dropdown) -> Item that is selected
 *   Button - submit the form to add to the cart
 *   
 */

const Item = (props) => {
  const { itemName, count } = props
  return (
    <div>My Added Item: {itemName} - Quantity: {count}</div>
  )
}

const Summary = (props) => {
  const { cartCount } = props;
  return (
    <div>
      Items in Cart: {cartCount}
    </div>
  )
}

const CartContents = (props) => {
  const { items } = props;
  let cartCount = 0; 
  Object.values(items).forEach((val) => {
    cartCount = cartCount + val
  })
  return (
    <div>
      <ul>
        {Object.keys(items).map((productName) => {
          return (
            <li><Item itemName={productName} count={items[productName]} /></li>
          )
        })}
      </ul>
      <Summary cartCount={cartCount} />
    </div>
  )
}

const ProductSelection = (props) => {
  const { products, setSelectedProduct, selectedProduct, setSelectedProductCount, selectedProductCount, setAddedItems, addedItems } = props;
  const onProductSelectChange = (event) => {
    setSelectedProduct(event.target.value)
  }

  const onProductCountChange = (event) => {
    setSelectedProductCount(event.target.value)
  }

  const onButtonClick = () => {
    console.log('added Items in product selection', addedItems)
    const newAddedItems = { ...addedItems };
    if(newAddedItems[selectedProduct]) {
      newAddedItems[selectedProduct] = Number(addedItems[selectedProduct]) + Number(selectedProductCount)
    } else {
      newAddedItems[selectedProduct] = Number(selectedProductCount)
    }
    setAddedItems(newAddedItems)
  }

  return (
    <div>
      <input type="number" onChange={onProductCountChange} value={selectedProductCount}/>
      <select value={selectedProduct} onChange={onProductSelectChange}>
        <option value=''>-- No Product Selected --</option>
        {products.map((product) => {
          return (
            <option value={product.name}>Product: {product.name}</option>
          )
        })}
      </select>
      <button onClick={onButtonClick}>Add To Cart</button>
    </div>
  )
}
const ProductForm = (props) => {
  const { products, setSelectedProduct, selectedProduct, setSelectedProductCount, selectedProductCount, setAddedItems, addedItems } = props
  
  return (
    <ProductSelection
      products={products}
      setSelectedProduct={setSelectedProduct} 
      selectedProduct={selectedProduct}
      setSelectedProductCount={setSelectedProductCount}
      selectedProductCount={selectedProductCount}
      setAddedItems={setAddedItems}
      addedItems={addedItems}
    />
  )

}

export const Marketplace = () => {
  const [availableProducts, setAvailableProducts] = useState([])
  const [selectedProduct, setSelectedProduct] = useState('')
  const [selectedProductCount, setSelectedProductCount] = useState('0')
  const [addedItems, setAddedItems] = useState({})

  /**
   * {
   *   productName: count
   * }
   */

  console.log('my selected product', selectedProduct)
  console.log('my selected count', selectedProductCount)
  console.log('my added items', addedItems)
  useEffect(() => {
    setAvailableProducts(getProducts())
  }, [])

  return (
    <div>
      <div style={{ marginBottom: '100px', border: "1px solid red"}}>
        <CartContents items={addedItems} />
      </div>
      <ProductForm
        products={availableProducts}
        setSelectedProduct={setSelectedProduct}
        selectedProduct={selectedProduct}
        setSelectedProductCount={setSelectedProductCount}
        selectedProductCount={selectedProductCount}
        setAddedItems={setAddedItems}
        addedItems={addedItems}
      />
    </div>
  )
}