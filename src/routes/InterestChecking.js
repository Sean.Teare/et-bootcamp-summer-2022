import React, { useState, useEffect } from 'react';
import { PageSections } from '../components/PageSections';
import { fetchContentChecking } from '../services/content';
import { flattenPageContent } from '../utils/content';

export const InterestChecking = () => {
  const [content, setContent] = useState({})
  useEffect(() => {
    (async () => setContent(flattenPageContent(await fetchContentChecking())))()
  }, [setContent])
  return (
    <div>
      <PageSections content={content} />
    </div>

  )
}
