import React, { useState, useEffect } from 'react';
import { PageSections } from '../components/PageSections';
import { fetchContentSavings } from '../services/content';
import { flattenPageContent } from '../utils/content';

export const OnlineSavings = () => {
  const [content, setContent] = useState({})

  useEffect(() => {
    (
      async () => {
        const rawContent = await fetchContentSavings()
        setContent(flattenPageContent(rawContent))
      })()
  }, [setContent])
  return (
    <div>
      <PageSections content={content} />
    </div>
  )
}
