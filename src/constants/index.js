export const CONTENTFUL_CONFIG = {
    space: 'jg7u5zv9poxf',
    accessToken: '',
    environment: 'seans-env',
    timeout: 3000,
    retryOnError: true,
    retryLimit: 3,
    resolveLinks: true,
    removeUnresolved: true,
    requestLogger: console.log,
    responseLogger: console.log
  }
  
  export const CONTENTFUL_PAGE_ENTITY_ID = '42TSvhsBNYlLIEEn1aogNz';
  export const CONTENTFUL_SITE_ENTITY_ID = '59a2vcwJUx4echc8d5wbIw';
  export const CONTENTFUL_ENTITY_ID_INTEREST_CHECKING = '6f98Vo5ac9v4tmZRDtCs8h';
  export const CONTENTFUL_ENTITY_ID_ONLINE_SAVINGS = '4vV65xyIJhBOTN96PgngfI';
  export const CONTENTFUL_ENTITY_ID_CD_HIGH_YIELD = '1oKNkImvF87BSLD7okCtQ2';
  export const CONTENTFUL_ENTITY_ID_MONEY_MARKET = '46ierBtxKWxh5AziJvljVc';
  export const CONTENTFUL_GRAPHQL_API_HOSTNAME = 'https://graphql.contentful.com/content/v1/';
  