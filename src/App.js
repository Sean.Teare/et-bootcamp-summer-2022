import React from 'react'
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { HighYieldCD } from './routes/HighYieldCD';
import { InterestChecking } from './routes/InterestChecking';
import { MoneyMarket } from './routes/MoneyMarket';
import { OnlineSavings } from './routes/OnlineSavings';
import { Form } from './routes/Form';
import { Marketplace } from './routes/Marketplace'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/online-savings-account" render={() => <OnlineSavings />}></Route>
          <Route path="/high-yield-cd" render={() =><HighYieldCD />} />
          <Route path="/interest-checking-account" render={() => <InterestChecking />} />
          <Route path="/money-market-account" render={() => <MoneyMarket />} />
          <Route path="/form" render={() => <Form />} />
          <Route path="/marketplace" render={() => <Marketplace />} />
          <Route path="/" render={() => <OnlineSavings />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
