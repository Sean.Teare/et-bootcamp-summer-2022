import React from 'react'
import { MenuLinks as Menu } from '../MenuLinks';

export const PageShell = ({ children, content = {} }) => {
	return (
		<div>
			<div>
				<Menu />
			</div>
			<div>
				{children}
			</div>
			<div>

			</div>
		</div>
	);
}