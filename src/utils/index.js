export const log = {
    trace: (msg, data) => { console.log('APP.TRACE: ', msg, data); },
    debug: (msg, data) => { console.log('APP.DEBUG: ', msg, data); },
    warn: (msg, data) => { console.log('APP.WARN: ', msg, data); },
    error: (msg, data) => { console.log('APP.ERROR: ', msg, data); }
  }
