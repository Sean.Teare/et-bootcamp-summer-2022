// assumption is that a page has a fixed data schema here.
export const flattenPageContent = (contentObj = {}) => {
	console.log('my content object', contentObj)
	const res = !!contentObj.fields ? contentObj.fields : {};
	if (res.pocPageSections) {
		res.pocPageSections = res.pocPageSections.map((section) =>
			formatSectionByType(section.sys.contentType.sys.id, section)
		);
	}
	return res;
}

export const formatSectionByType = (contentType, content) => {
	switch (contentType) {
		case 'pocFeaturesSection':
			return {
				...content.fields,
				items: content.fields.pocFeatureItems.map((item) => {
					return {
						...item.fields,
						pocFeatureItemImage: {
							...item.fields.pocFeatureItemImage.fields,
							url: item.fields.pocFeatureItemImage.fields.file.url,
						}
					}
				}),
				contentType,
			}
		case 'pocBankBetter':
			return {
				...content.fields,
				items: content.fields.bankBetterItems.map((item) => {
					return {
						...item.fields,
						headerImage: {
							...item.fields.headerImage.fields,
							url: item.fields.headerImage.fields.file.url,
						}
					}
				}),
				contentType,
			}
		case 'pocFooter':
			return {
				...content.fields,
				items: (!content.fields || !content.fields.pocFooterBullets ? [] : content.fields.pocFooterBullets)
					.sort((item, prevItem) => item.fields.order - prevItem.fields.order)
					.map((item) => {
						return {
							...item.fields,
						}
					}),
				contentType,
			}
		case 'pocHeader':
			console.log('pocHeader fields', content.fields)
			return {
				...content.fields,
				linksLeft: content.fields.linksLeft.fields.links.map((item) => ({ ...item.fields })),
				linksRight: content.fields.linksRight.fields.links.map((item) => ({ ...item.fields })),
				navigation: content.fields.navigation.map((nav) => {
					return {
						...nav.fields,
						items: nav.fields.links.map((link) => {
							return {
								...link.fields,
							};
						}),
					};
				}),
				contentType,
			}
		default:
			return {
				...content,
				contentType,
			}
	}
}
