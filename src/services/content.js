
import { omitBy, isUndefined } from 'lodash';
import { CONTENTFUL_CONFIG, CONTENTFUL_GRAPHQL_API_HOSTNAME } from '../constants';
import { createClient } from 'contentful';
import * as constants from '../constants';

const contentPageMap = {
  'HIGH_YIELD_CD': constants.CONTENTFUL_ENTITY_ID_CD_HIGH_YIELD,
  'MONEY_MARKET': constants.CONTENTFUL_ENTITY_ID_MONEY_MARKET,
  'SAVINGS': constants.CONTENTFUL_ENTITY_ID_ONLINE_SAVINGS,
  'CHECKING': constants.CONTENTFUL_ENTITY_ID_INTEREST_CHECKING,
};
const client = createClient(CONTENTFUL_CONFIG);
let nextSyncToken;
export const fetchContentById = async (entryId) => client.getEntry(entryId, { include: 10 });
export const fetchContentByPage = async (pageName) => fetchContentById(contentPageMap[pageName]);
export const fetchContentMoneyMarket = async () => fetchContentByPage('MONEY_MARKET');
export const fetchContentSavings = async () => fetchContentByPage('SAVINGS');
export const fetchContentChecking = async () => fetchContentByPage('CHECKING');
export const fetchContentHighYieldCD = async () => fetchContentByPage('HIGH_YIELD_CD');

export const syncContent = async () => {
  const response = await client.sync(omitBy({ initial: isUndefined(nextSyncToken), nextSyncToken }, isUndefined));
  ({ nextSyncToken } = response);
  return response;
}
export const fetchContentWithoutSections = async () => {
  const query = {
    query: 'query{page(id: "42TSvhsBNYlLIEEn1aogNz"){title subtitle text}}'
  }
  const res = await fetch(
    `${CONTENTFUL_GRAPHQL_API_HOSTNAME}/spaces/${CONTENTFUL_CONFIG.space}/environments/${CONTENTFUL_CONFIG.environment}`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${CONTENTFUL_CONFIG.accessToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(query),
    }
  );
  return res;
}

export const getProducts = () => {
  return [
    {
      name: 'Online Savings'
    },
    {
      name: 'Checking'
    },
    {
      name: 'Money Market'
    },
    {
      name: 'High-Yield CD',
      options: [
        {
          name: '6 months',
          rate: '0.5'
        },
        {
          name: '1 year',
          rate: '1.0'
        },
        {
          name: '5 year',
          rate: '2.0'
        }
      ]
    }
  ]
}